<?php
include_once('MainInterface.php');
class Notifier {
  protected $mailer;

  public function __construct(MainInterface $mailer)
  {
    $this->mailer = $mailer;
  }

  public function send() {
    $this->mailer->execute();
  }
}
